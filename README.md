# Back-end test

First of all, welcome!

This is the [Reload Health](https://reload.co) test for back-end positions.

### Summary

- [Stack](#stack)
- [Steps](#Steps)
- [Description](#description) (Here you will find what is required from this test)
- [Dataset](#dataset)
    - [Data structure](#data-structure)


## Stack

We expect this test to be written according to this technology stack:

### Core
- NodeJS `14.x` (you can pick `JavaScript` or `TypeScript`, it's on you)
- MySQL `5.7`
- Restify
- Knex
- Jest
- Redis
- Docker

### stack-bonus
This is not required in this test but will count positively if you use/implement it.

- docker-compose
- CI/CD (Gitlab ci if you're hosting at Gitlab and Github actions if you're using Github)
- Sentry
- Swagger
- Grafana
- Prometheus
- Terraform

## Steps
You should:
- Download the dataset
- Create a repository in Github/Gitlab and host the code there
- Use `reload-test` as password in database for root user
- Use the ports for the services:
    - Server: 5000
    - MySQL: 3306
    - Redis: 6379
- Modeling the database according to [data structure](#data-structure) and letting the model in project folder
- Mention and describe in the README file if you used any technology described at [stack bonus](#stack-bonus)
- Provide an `Insomnia` backup file to test the api
- **Don't forget to let all .env variables and values available in a .env.example file**

## Description
This section will describe what we expect from you in this test.

Your project should contain:
- Containerized API and databases
- At least one unit test and one integration test

You should create a restful API containing:
- A seed using knex to insert all the dataset to the DataBase
- A cache to the DataBase queries (use Redis), please in method let a console log to mention when the data is provided from database or from cache.
    - Cache expiration should be 1 hour
- The databases creation should use the `factory design pattern` and its connection should use `singleton design pattern`. (Note: if you pick TypeScript use an ENUM to set MySQL as the currently used database)

The restful api have to contain these routes:
- companies
    - GET all
    - GET one by id
    - GET one by name
    - UPDATE its partial information (name, suffix etc)
    - DELETE it
- computers
    - GET all
    - GET all contained in one company
    - CREATE
    - DELETE
- contributors
    - GET all
    - GET all contained in one company
    - CREATE
    - UPDATE its partial information (age, jobTitle etc)
    - DELETE

**Note:.** The dataset was randomly generated, so you have some id conflicts. In this case, you can pick one of these solutions and then mention which one you took at your project's README:
- Insert all companies reindexing with auto-incremental index
- Insert all companies and just reindex that ones with conflict in id
- Use UUID to index

#### Bonus description
If you chose to implement stack bonus-related technologies, please mention in README which was implemented.

For `docker-compose`: Imagine you have a VM in any cloud provider where you will deploy all project and with a docker-compose kickstart all application should be initialized, linked, and in an intern network.

For `GitLab CI`/`GitHub actions`: Imagine you have a VM in GCP and you should test, build and then upload via SCP the .js files to the VM. (more described and useful steps will be appreciated).

For `Sentry`: let a variable in .env for the DSN, we will use ours to test. Your effort should be to track major errors.

For `Terraform`: feel free to provide the structure you think is the best, just make sure to write it containing at least GCP structure.

Other technologies and/or design patterns are welcome and will be considered.

## Dataset
Get the dataset in <a target="_blank" href="https://gist.githubusercontent.com/hfabio/514717af3461ced9947641d583c29581/raw/fb3d702f286952a169bf460826b0de1a16e89af3/data.json">this</a> url.

### Data structure
This is an example of data contained in the [dataset](#dataset)
```json
{
   "id":40068,
   "business_name":"Schuppe - Spencer",
   "suffix":"LLC",
   "industry":"Tactics",
   "catch_phrase":"Compatible background benchmark",
   "bs_company_statement":"grow global infrastructures",
   "logo":"http://placeimg.com/640/480/business",
   "type":"enable",
   "phone_number":"803.658.4521",
   "full_address":"2037 Champlin Summit",
   "latitude":"-73.1783",
   "longitude":"55.6623",
   "contributors":[
      {
         "firstName":"Hailee",
         "lastName":"Davis",
         "title":"Corporate Security Liaison",
         "jobTitle":"Legacy Implementation Strategist",
         "age":23
      },
      {
         "firstName":"Clare",
         "lastName":"Hauck",
         "title":"Future Metrics Administrator",
         "jobTitle":"Central Integration Representative",
         "age":35
      },
      {
         "firstName":"Elvis",
         "lastName":"Nitzsche",
         "title":"District Interactions Planner",
         "jobTitle":"International Accounts Associate",
         "age":31
      },
      {
         "firstName":"Randall",
         "lastName":"Rau",
         "title":"District Data Supervisor",
         "jobTitle":"Central Group Associate",
         "age":18
      },
      {
         "firstName":"Leda",
         "lastName":"Kirlin",
         "title":"Global Integration Technician",
         "jobTitle":"Human Data Associate",
         "age":35
      }
   ],
   "desktops":[
      {
         "id":72118,
         "platform":"windows",
         "type":"workstation",
         "os":"Windows 10",
         "ip":"200.65.189.93"
      },
      {
         "id":23121,
         "platform":"unix",
         "type":"server",
         "os":"Windows 10",
         "ip":"175.212.98.24"
      },
      {
         "id":6716,
         "platform":"unix",
         "type":"workstation",
         "os":"Ubuntu Desktop 21.04",
         "ip":"0.118.144.60"
      },
      {
         "id":80340,
         "platform":"windows",
         "type":"server",
         "os":"RHEL 7.7",
         "ip":"223.46.102.210"
      },
      {
         "id":79883,
         "platform":"unix",
         "type":"server",
         "os":"Windows 11",
         "ip":"130.175.71.8"
      }
   ]
}
```
